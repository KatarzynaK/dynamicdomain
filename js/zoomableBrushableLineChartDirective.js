angular.module('zoomBrushLine', ['dyndom'])
.directive('zoomBrushLineChart', function() {
	return {
		restrict: 'E',
		replace: false,
		link: function(scope, element, attrs) {

			var margin = {top: 10, right: 10, bottom: 100, left: 40},
				margin2 = {top: 530, right: 10, bottom: 20, left: 40},
				width = 1200 - margin.left - margin.right,
				height = 600 - margin.top - margin.bottom,
				height2 = 600 - margin2.top - margin2.bottom;

			var parseDate = d3.time.format("%Y-%m-%d").parse;
				 
			var x = d3.time.scale().range([0, width]),
				x2 = d3.time.scale().range([0, width]),
				y = d3.scale.linear().range([height, 0]),
				y2 = d3.scale.linear().range([height2, 0]);
				 
			var xAxis = d3.svg.axis().scale(x).orient("bottom"),
				xAxis2 = d3.svg.axis().scale(x2).orient("bottom"),
				yAxis = d3.svg.axis().scale(y).orient("left")
					.innerTickSize(-width)
					.outerTickSize(0)
					.tickPadding(10);
				 
			var brush = d3.svg.brush()
				.x(x2)
				.on("brush", brush);
				 
			var line = d3.svg.line()				    
				.x(function(d) { return x(d.date); })
				.y(function(d) { return y(d.value); });
				 
			var line2 = d3.svg.line()
				.x(function(d) {return x2(d.date); })
				.y(function(d) {return y2(d.value); });
				 
			var svg = d3.select(element[0]).append("svg")
				.attr("width", width + margin.left + margin.right)
				.attr("height", height + margin.top + margin.bottom);
				 
			svg.append("defs").append("clipPath")
				.attr("id", "clip")
				.append("rect")
				.attr("width", width)
				.attr("height", height);
				 
			var focus = svg.append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
				      
			var context = svg.append("g")
				.attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

			var zoom = d3.behavior.zoom().scaleExtent([1, 100])
				.center([width / 2, height / 2])
				.on("zoom", draw);

			// Add rect cover the zoomed graph and attach zoom event.
			var rect = svg.append("svg:rect")
				.attr("class", "pane")
				.attr("width", width)
				.attr("height", height)
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
				.call(zoom)
				// disable panning
				.on("mousedown.zoom", null)
				.on("touchstart.zoom", null)
				.on("touchmove.zoom", null)
				 .on("touchend.zoom", null);
				 
			d3.csv("data/histogram.csv", function(error, data) {

				data.sort(function(x, y){
	   				return d3.ascending(x.date, y.date);
				})
				 
				data.forEach(function(d) {
				    d.date = parseDate(d.date);
				});

				x.domain(d3.extent(data, function(d) { return d.date; }));
				y.domain([0, d3.max(data, function(c) { return c.value; })]);
				x2.domain(x.domain());
				y2.domain(y.domain());

				zoom.x(x);
				      
				var focuslines = focus
				    .append("path")
				    .datum(data)
				    .attr("class","line")
				    .attr("d", line)
				    .style("stroke", "steelblue")
				    .attr("clip-path", "url(#clip)");
				    
				focus.append("g")
				    .attr("class", "x axis")
				    .attr("transform", "translate(0," + height + ")")
				    .call(xAxis);
				 
				focus.append("g")
				    .attr("class", "y axis")
				    .call(yAxis);
				    
				var contextLines = context
				    .append("path")
				    .datum(data)
				    .attr("class", "line")
				    .attr("d", line2)
				    .style("stroke", "steelblue")
				    .attr("clip-path", "url(#clip)");
				 
				context.append("g")
				    .attr("class", "x axis")
				    .attr("transform", "translate(0," + height2 + ")")
				   	.call(xAxis2);
				 
				context.append("g")
				    .attr("class", "x brush")
				    .call(brush)
				    .selectAll("rect")
				    .attr("y", -6)
				    .attr("height", height2 + 7);
						
			});
				 
			function brush() {
				x.domain(brush.empty() ? x2.domain() : brush.extent());
				focus.selectAll("path.line").attr("d",  line);
				focus.select(".x.axis").call(xAxis);
				focus.select(".y.axis").call(yAxis);	
			}

			function draw() {
				focus.select(".line").attr("d", line);
				focus.select(".x.axis").call(xAxis);
				brush.extent(x.domain());
				svg.select(".brush").call(brush);
			}

		}
	}
});