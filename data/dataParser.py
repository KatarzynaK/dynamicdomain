import csv
from datetime import date
histogram = dict()
out = []
# file too big to store in the repository, available at https://www.kaggle.com/START-UMD/gtd
with open('terrorism.csv', newline='') as file:
	reader = csv.reader(file, delimiter=',')
	file.readline()
	for row in reader:
		year = row[1]
		month = row[2]
		day = row[3]
		# wrongly formatted dates are added to the nearest bin...
		if(day == 0):
			day += 1
		if(month == 0):
			month += 1
		try:	
			datex = date(int(row[1]), int(row[2]), int(row[3]))
			if datex in histogram:
				histogram[datex] += 1
			else:
				histogram[datex] = 1
		except ValueError:
			# or ommit if no other option
			continue
	for key in histogram:
		out.append({'date': key, 'value': histogram[key]})

	# adding missing values for 1993 so that the chart show zero instead of interpolating
	# input data is sorted before plotting anyway so the order does not matter
	for month in range(1, 12):
		out.append({'date': date(1993, month, 1), 'value': 0})

with open('histogram.csv', 'w', newline='') as ofile:
    fields = ['date', 'value']
    writer = csv.DictWriter(ofile, fields)
    writer.writeheader()
    writer.writerows(out)


